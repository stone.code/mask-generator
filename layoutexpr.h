/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#ifndef _LAYOUTEXPR_H_
#define _LAYOUTEXPR_H_

#include <stdexcept>

class parse_error : public std::exception {
public:
	parse_error( char const* lex, char const* what )
	: _lex(lex), _what(what) {};

	char const* what() const throw() { return _what; };
	char const* lex() const throw() { return _lex; };
private:
	char const* _what;
	char const* _lex;
};

extern void load_config( char const* );
extern void load_layout( char const* );
extern void save_layout( char const*, char format );
extern void parse_and_execute_expression( char const* );
extern void copy_unreferenced_layers();

#endif // _LAYOUTEXPR_H_
