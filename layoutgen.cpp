/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#include "layoutexpr.h"
#include "expatpp.h"
#include "layout.h"
#include <algorithm>
#include <iostream>
#include <vector>
#include <boost/program_options.hpp>

using std::string;
using std::vector;
namespace po = boost::program_options;

string		in_filename;
string		out_filename;
char		out_format = 'G';
string		config_filename;
vector<string>	expressions;
bool		verbose = false;
bool		auto_copy_layers = false;

static bool check_out_format()
{
	out_format = toupper( out_format );
	switch ( out_format ) {
		case 'G':	// GDSII
		case 'C':	// CIF
		case 'D':	// DXF
			return false;
		default:
			return true;
	}
}

int main( int argc, char* argv[] )
{
	po::options_description options("Allowed options");
	options.add_options()
		("help,h", "Produce a help message")
		("version,v", "Produce a version message")
		("verbose", po::value<bool>(&verbose)->zero_tokens(), "Produce verbose messages during processing.")
		("in,i", po::value<string>(&in_filename), "Input filename")
		("out,o", po::value<string>(&out_filename), "Output filename")
		("format,f", po::value<char>(&out_format)->default_value('G'), "Output format (GDSII, CIF, or DXF)" )
		("config,c", po::value<string>(&config_filename), "Configuration filename")
		("auto-copy,a", po::value<bool>(&auto_copy_layers)->zero_tokens(), "Automatically copy layers from input layout to output layout." )
		("expr,e", po::value<vector<string> >(&expressions), "Mask expressions");
	po::positional_options_description pd;
	pd.add( "expr", -1 );
	
	po::variables_map vm;
	po::store( po::command_line_parser( argc, argv ).options(options).positional(pd).run(), vm );
	po::notify( vm );
	
	if ( vm.count("help") ) 
	{
		std::cout << options << std::endl;
		return EXIT_SUCCESS;
	}
	if ( vm.count("version") )
	{
		std::cout << "Version " PACKAGE_VERSION << '\n';
		std::cout << "\tusing " << liblayout_version_zstring() << '\n';
		std::cout << "\tusing " << xml::expat_version() << std::endl;
		return EXIT_SUCCESS;
	}
	
	if ( in_filename.empty() ) throw po::error( "option 'in' is required" );
	if ( out_filename.empty() ) throw po::error( "option 'out' is required" );
	if ( check_out_format() ) throw po::error( "option 'format' is incorrect (use a single letter: G, C, or D)" );
	if ( config_filename.empty() ) throw po::error( "option 'config' is required" );
	
	// Load the layout file
	load_config( config_filename.c_str() );
	load_layout( in_filename.c_str() );
	// Start running the expressions
	for ( vector<string>::const_iterator lp = expressions.begin(); lp != expressions.end(); ++lp ) {
		char const* lex = lp->c_str();

		try {
			parse_and_execute_expression( lex );
		}
		catch ( parse_error& e ) {
			std::cout << "Error parsing expression at position " << (e.lex() - lex) << ".  " << e.what() << std::endl;
			return EXIT_FAILURE;
		}
	}
	// Perform auto-copy of layers
	if ( auto_copy_layers ) copy_unreferenced_layers();
	// Save the result
	save_layout( out_filename.c_str(), out_format );

	return EXIT_SUCCESS;
}
