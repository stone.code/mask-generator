AC_DEFUN(AC_UNDEFINE,[
cp confdefs.h confdefs.h.tmp
grep -v $1 < confdefs.h.tmp > confdefs.h
rm confdefs.h.tmp
])

AC_DEFUN(AC_CXX_CHECK_LIB,[
saved_ldflags="${LDFLAGS}"
LDFLAGS="${saved_ldflags} -l$1"
AC_MSG_CHECKING([for -l$1])
AC_LINK_IFELSE([$2],
	[AC_MSG_RESULT([yes])]
	[$3],
	[AC_MSG_RESULT([no])]
	[$4])
LDFLAGS="${saved_ldflags}"
])
