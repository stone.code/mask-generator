/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#include "layoutexpr.h"
#include <assert.h>
#include <deque>
#include <gmpxx.h>
#include <layout.h>
#include <ctype.h>
#include <stdexcept>
#include <vector>
#include <boost/polygon/polygon.hpp>

extern void read_layers( char const* filename, liblayout::cif::map_name_id& name_to_id, liblayout::cif::map_id_name& id_to_name );
extern bool verbose;

using std::string;
using std::vector;
using namespace liblayout;
using namespace boost::polygon::operators;

/************************************************************************
** liblayout
*/

std::list<layout>	layouts;
layout*			top_layout = 0;
double			gdsii_scale = 1e-9;
layout			out_layout;

cif::map_name_id	name_to_id;
cif::map_id_name	id_to_name;

/************************************************************************
** boost::polygon
*/

typedef std::deque<polygon> polygon_set;

namespace boost { namespace polygon {
	template <>
	struct geometry_concept<liblayout::coord_t> { typedef coordinate_concept type; };
	template <>
	struct coordinate_traits<liblayout::coord_t> {
		typedef liblayout::coord_t cordinate_type;
		typedef long double area_type;
		typedef long long manhattan_area_type;
		typedef unsigned long long unsigned_area_type;
		typedef long long coordinate_difference;
		typedef long double coordinate_distance;
	};
	template <>
	struct high_precision_type<liblayout::coord_t> {
		typedef mpz_class type;
		
		static inline liblayout::coord_t convert( mpz_class const& v ) {
			return v.get_si();
		}
	};
	liblayout::coord_t convert_high_precision_type( mpz_class const& a ) {
		return a.get_si();
	}
	liblayout::coord_t convert_high_precision_type( long double a ) {
		return static_cast<liblayout::coord_t>(a);
	}
 	template <>
	struct geometry_concept<liblayout::point> { typedef point_concept type; };
	template <>
	struct point_traits<liblayout::point> {
		typedef liblayout::coord_t coordinate_type;
		static inline coordinate_type get( point const& rhs, orientation_2d orient ) {
			return (orient==HORIZONTAL) ? rhs.x : rhs.y;
		}
	};
	template <>
	struct point_mutable_traits<liblayout::point> {
		static inline void set( liblayout::point& lhs, orientation_2d orient, liblayout::coord_t value ) {
			if (orient==HORIZONTAL)
				lhs.x = value;
			else
				lhs.y = value;
		}
		static inline liblayout::point construct( liblayout::coord_t x, liblayout::coord_t  y ) {
			return liblayout::point( x, y );
		}
	};
	template <>
	struct geometry_concept<liblayout::polygon> { typedef polygon_concept type; };
	template <>
	struct polygon_traits<liblayout::polygon> {
		typedef liblayout::coord_t coordinate_type;
		typedef liblayout::polygon::const_iterator iterator_type;
		typedef liblayout::point point_type;
		typedef liblayout::polygon polygon_type;
		
		static inline iterator_type begin_points( polygon_type const& t ) { return t.begin(); }
		static inline iterator_type end_points( polygon_type const& t ) { return t.end(); }
		static inline std::size_t size( polygon_type const& t ) { return t.size(); }
		static inline winding_direction winding( polygon_type const& t ) { return unknown_winding; }
	};
	template <>
	struct polygon_mutable_traits<liblayout::polygon> {
		typedef liblayout::polygon polygon_type;
		
		template <typename It>
		static inline polygon_type& set_points( polygon_type & t, It begin, It end ) {
			t.clear();
			while ( begin!=end ) {
				liblayout::point pt( begin->x(), begin->y() );
				t.push_back( pt );
				++begin;
			}
			return t;
		}
	};
	
	template <>
	struct geometry_concept<polygon_set> { typedef polygon_set_concept type; };
	template <>
	struct polygon_set_traits<polygon_set> {
		typedef liblayout::coord_t coordinate_type;
		typedef ::polygon_set::const_iterator iterator_type;
		typedef ::polygon_set polygon_set;
		typedef ::polygon_set operator_arg_type;
		
		static inline iterator_type begin( polygon_set const& ps ) { return ps.begin(); }
		static inline iterator_type end( polygon_set const& ps ) { return ps.end(); }
		static inline bool clean( polygon_set const& ps ) { return false; }
		static inline bool sorted( polygon_set const& ps ) { return false; }
	};
	template <>
	struct polygon_set_mutable_traits<polygon_set> {
		typedef ::polygon_set polygon_set;

		template <typename It>
		static inline void set( polygon_set& ps, It begin, It end ) {
			ps.clear();
			polygon_set_data<liblayout::coord_t> tmp;
			tmp.insert( begin, end );
			tmp.get( ps );
		}
	};
} }



/************************************************************************
** Glue between liblayout and boost::polygon
*/

struct format_layer_id
{
public:
	format_layer_id( liblayout::layer_id id ) : _id(id) {}
	format_layer_id( format_layer_id const& rhs ) : _id(rhs._id) {}
	operator liblayout::layer_id() const { return _id; }
	liblayout::layer_id _id;
};

static std::ostream& operator<<( std::ostream& out, format_layer_id id )
{
	out << '#' << static_cast<long>(id);
	cif::map_id_name::const_iterator it = id_to_name.find( id );
	if ( it != id_to_name.end() ) std::cout << " '" << it->second << "'";
	return out;
}

static void convert( polygon_set& ps, liblayout::layer_id id )
{
	assert( top_layout );
	
	ps.clear();

	std::vector<shape> shapes;
	top_layout->get_shapes( shapes, id );
	
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp ) {
		liblayout::polygon rhs;
		lp->get_polygon( rhs );
		ps.push_back( rhs );
	}
	assert( shapes.size() == ps.size() );
}

static void convert( liblayout::layout& out, liblayout::layer_id id, polygon_set const& ps )
{
	for ( polygon_set::const_iterator lp = ps.begin(); lp != ps.end(); ++lp ) {
		out.insert( id, *lp );
	}
}

/************************************************************************
** Parser
*/

static char const* advance_space( char const* lex )
{
	assert( lex );
	while ( isspace(*lex) ) ++lex;
	return lex;
}

static char const* advance_char( char const* lex, char token, char const* error_msg )
{
	lex = advance_space( lex );
	assert( lex );
	if ( *lex != token ) throw parse_error( lex, error_msg );
	return lex+1;
}

static char const* advance_uint( char const* lex, unsigned& lhs )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	if ( !isdigit(*lex ) ) throw parse_error( lex, "Expecting integer expression." );
	
	unsigned ret = *lex - '0';
	while ( isdigit( *++lex ) ) { ret = ret*10 + (*lex-'0'); }
	lhs = ret;
	return lex;
}

static layer_id get_next_layer_id( cif::map_name_id const& m )
{
	// Find the maximum layer_id in the map
	layer_id max_id = 0;
	cif::map_name_id::const_iterator lp;
	for ( lp = m.begin(); lp != m.end(); ++lp ) {
		layer_id tmp = lp->second;
		if ( tmp > max_id ) max_id = tmp;
	}
	// max_id should now hold the maximum currently used id number

	// We need the next id
	return max_id + 1;
}

static char const* advance_maskid( char const* lex, layer_id& id, bool allow_new = false )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	if ( *lex == '@' ) {
		// Number identifier
		unsigned tmp;
		lex = advance_uint( lex+1, tmp );
		id = static_cast<layer_id>( tmp );
	}
	else if ( isalpha(*lex) ) {
		char const* lex_begin = lex;
		++lex;
		while ( isalnum(*lex) ) ++lex;
		string name( lex_begin, lex );
		cif::map_name_id::const_iterator lp = name_to_id.find( name );
		if ( lp == name_to_id.end() ) {
			// The name was not found
			if ( allow_new ) {
				layer_id new_id = get_next_layer_id( name_to_id );
				name_to_id.insert( std::make_pair( name, new_id ) );
				id_to_name.insert( std::make_pair( new_id, name ) );
				id = new_id;
			}
			else {
				// Can't create the name
				// This is an error
				throw parse_error( lex, "The mask name was not recognized." );
			}
		}
		else {
			// Found the name
			id = lp->second;
		}
	}
	else {
		throw parse_error( lex, "Expected mask id expression." );
	}

	return lex;
}

static char const* advance_expr( char const* lex, polygon_set& ps );

static char const* advance_factor( char const* lex, polygon_set& ps )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	if ( *lex=='(' ) {
		++lex;
		lex = advance_expr( lex , ps);
		assert( lex );
		lex = advance_char( lex, ')', "Expected matching bracked." );
		assert( lex );
	}
	else {
		layer_id id;
		lex = advance_maskid( lex, id );
		assert( lex );
		convert( ps, id );
	}

	return lex;
}

static char const* advance_term( char const* lex, polygon_set& ps )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	lex = advance_factor( lex, ps );
	assert( lex );

	do {
		lex = advance_space( lex );
		assert( lex );

		if ( *lex=='*' ) {
			++lex;
			polygon_set tmp;
			lex = advance_factor(lex,tmp);
			ps *= tmp;
		}
		else if ( *lex=='^') {
			++lex;
			polygon_set tmp;
			lex == advance_factor(lex,tmp);
			ps ^= tmp;
		}
		else {
			return lex;
		}
	} while(1);
}

static char const* advance_expr( char const* lex, polygon_set& ps )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	lex = advance_term( lex, ps );
	assert( lex );

	do {
		lex = advance_space( lex );
		assert( lex );
		
		if ( *lex=='+' ) {
			++lex;
			lex = advance_space( lex );
			if ( isdigit(*lex) ) {
				unsigned value;
				lex = advance_uint( lex, value );
				boost::polygon::bloat( ps, value );
			}
			else {
				polygon_set tmp;
				lex = advance_term( lex, tmp );
				ps += tmp;
			}
		}
		else if (*lex=='-') {
			++lex;
			lex = advance_space( lex );
			if ( isdigit(*lex) ) {
				unsigned value;
				lex = advance_uint( lex, value );
				boost::polygon::shrink( ps, value );
			}
			else {
				polygon_set tmp;
				lex = advance_term( lex, tmp );
				ps -= tmp;
			}
		}
		else {
			return lex;
		}
	} while(1);
}

void parse_and_execute_expression( char const* lex )
{
	layer_id lhs;

	lex = advance_maskid( lex, lhs, true );
	assert( lex );
	lex = advance_char( lex, '=', "Expected '='." );
	assert( lex );
	
	polygon_set ps;
	lex = advance_expr( lex, ps );
	assert( lex );
	lex = advance_space( lex );
	if ( *lex ) throw parse_error( lex, "Expected end of expression." );

	if ( verbose )	
		std::cout << "Assigning data to layer " << format_layer_id(lhs) << " with " << ps.size() << " polygons.\n";
	convert( out_layout, lhs, ps );
}

void load_config( char const* filename )
{
	read_layers( filename, name_to_id, id_to_name );
}

static void on_comment( void* _unused, char const* comment )
{
	if ( verbose )
		std::cout << comment << '\n';	
}

static void on_layer( void* _unused, char const* name, layer_id id )
{
	std::cout << "Unrecognized layer '" << name << "' added with an id of " << id << ".\n";
}

void load_layout( char const* filename )
{
	static const long dxf_import_scale = 1000;

	// import the layout
	if ( gdsii::is_file_gdsii( filename ) )
	{
		// gdsii file remembers scale
		top_layout = gdsii::import( layouts, &gdsii_scale, on_comment, 0, filename );
		if ( verbose )
			std::cout << "Input GDSII file reports scale is " << gdsii_scale << "." << std::endl;
	}
	else if ( cif::is_file_cif( filename ) )
	{
		top_layout = cif::import( layouts, name_to_id, 
			on_comment, 0, 
			on_layer, 0,
			filename );
		// don't know scale
		// most often, DB unit is 10 microns.
		gdsii_scale = 1e-8;
	}
	else if ( dxf::is_file_dxf( filename ) )
	{
		top_layout = dxf::import( layouts, name_to_id, dxf_import_scale,
			on_comment, 0,
			on_layer, 0,
			filename );
		// don't know scale
		// most often, one unit is a micron
		// we are multiplying by 1000, so DB unit is a nm.
		gdsii_scale = 1e-9;
	}
	else
	{
		std::cerr << "ERROR:  The input file must represent a layout in either CIF, GDSII, or DXF format." << std::endl;
		throw std::runtime_error( "Could not read the input layout file." );
		return;
	}
	assert( top_layout );
}

void copy_unreferenced_layers()
{
	std::set<layer_id> in, out;
	
	top_layout->get_layers( in );
	out_layout.get_layers( out );
	
	for ( std::set<layer_id>::const_iterator lp = in.begin(); lp != in.end(); ++lp ) {
		// do not copy over layers that were generated
		if ( out.find(*lp)!=out.end() ) continue;
		// Copy the layer
		if ( verbose )
			std::cout << "Copying shapes for layer " << format_layer_id(*lp) << '\n';
			
		vector<shape> shapes;
		top_layout->get_shapes( shapes, *lp );
		for ( vector<shape>::const_iterator s = shapes.begin(); s != shapes.end(); ++s ) {
			out_layout.insert( *s );
		}
	}
}

void save_layout( char const* filename, char format )
{
	switch ( format ) {
		case 'G':
			gdsii::write( out_layout, gdsii_scale, filename );
			break;
		case 'C':
			cif::write( out_layout, id_to_name, true, filename );
			break;
		case 'D':
			dxf::write( out_layout, id_to_name, 0.001, filename );
			break;
		default:
			assert( false );
	}
}
