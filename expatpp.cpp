/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2005-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains a thin C++ wrapper around the Expat library.
**
*/

#include "expatpp.h"
#include <assert.h>
#include <expat.h>
#include <stdexcept>
#include <string.h>
#include <sstream>
#include <iostream>

#ifdef WIN32
#define XMLCALLBACK __cdecl
#else
#define XMLCALLBACK
#endif	

class xml::parser::helper
{
public:
	static	void	XMLCALLBACK element_start_trampoline( void*, char const*, char const** );
	static	void	XMLCALLBACK element_end_trampoline( void*, char const* );
	static	void	XMLCALLBACK character_data_trampoline( void*, char const* text, int len );
	static	void	XMLCALLBACK comment_data_trampoline( void*, char const* text );
	static	void	XMLCALLBACK cdata_start_trampoline( void* );
	static	void	XMLCALLBACK cdata_end_trampoline( void* );
};

//**********************************************************
// xml::parser
//----------------------------------------------------------

void	xml::parser::parse( std::istream& in )
{
	char buffer[8192];

	// the parser is not reentrant
	if ( ptr ) throw std::logic_error( "Calls to xml parser are not reentrant." );

	// create the parser
	XML_Parser p = XML_ParserCreate(0);
	if ( !p ) throw std::bad_alloc();
	ptr = p;
	assert( parse_depth == 0 );

	// install the handlers
	XML_SetUserData( p, this );
	XML_SetElementHandler(p, helper::element_start_trampoline, helper::element_end_trampoline);
	XML_SetCharacterDataHandler( p, helper::character_data_trampoline );
	XML_SetCommentHandler( p, helper::comment_data_trampoline );
	XML_SetCdataSectionHandler( p, helper::cdata_start_trampoline, helper::cdata_end_trampoline );

	int done;

	do 
	{

		in.read( buffer, sizeof(buffer) );
		if ( in.fail() && !in.eof() )
		{
			XML_ParserFree(p);
			ptr = 0;
			throw xml::parse_error( "Error reading XML file", 0, 0 );
		}

		done = in.eof();

		if (! XML_Parse(p, buffer, in.gcount(), done)) 
		{
			// Save the information we need for the error message
			unsigned line = XML_GetCurrentLineNumber( p );
			unsigned column = XML_GetCurrentColumnNumber( p );
			enum XML_Error error_code = XML_GetErrorCode( p );

			// Free the parser
			XML_ParserFree(p);
			ptr = 0;

			// Create the exception
			throw xml::parse_error( XML_ErrorString(error_code), line, column );
		}

	} while ( !done );

	ptr = 0;
	assert( parse_depth == 0 );
	XML_ParserFree(p);
}

unsigned long xml::parser::current_byte_index() const
{
	assert( ptr );
	XML_Parser p = reinterpret_cast<XML_Parser>( ptr );
	long ret = XML_GetCurrentByteIndex( p );
	assert( ret >= 0 );
	return ret;
}

unsigned xml::parser::current_line() const
{
	assert( ptr );
	XML_Parser p = reinterpret_cast<XML_Parser>( ptr );
	int ret = XML_GetCurrentLineNumber( p );
	assert( ret >= 0 );
	return ret;
}

unsigned xml::parser::current_column() const
{
	assert( ptr );
	XML_Parser p = reinterpret_cast<XML_Parser>( ptr );
	int ret = XML_GetCurrentColumnNumber( p );
	assert( ret >= 0 );
	return ret;
}

void xml::parser::element_start(char const *name, char const **attr )
{
}

void xml::parser::element_end( char const* name )
{
}

void xml::parser::character_data( char const* text, int len  )
{
}

void xml::parser::comment_data( char const* text )
{
}

char const*	xml::parser::get_attribute( char const** attr, char const* name )
{
	while ( *attr )
	{
		if ( strcmp( *attr, name ) == 0 )
		{
			return *++attr;
		}

		++attr;
		if ( *attr ) ++attr;
	};

	return 0;
}


void xml::parser::helper::element_start_trampoline(void *data, const char *el, const char **attr) 
{
	assert( data );
	xml::parser* that = reinterpret_cast<xml::parser*>( data );
	++that->parse_depth;
	that->element_start( el, attr );
}

void xml::parser::helper::element_end_trampoline(void *data, const char *el) 
{
	assert( data );
	xml::parser* that = reinterpret_cast<xml::parser*>( data );
	that->element_end( el );
	--that->parse_depth;
}

void xml::parser::helper::character_data_trampoline(void *data, char const* text, int len ) 
{
	assert( data );
	xml::parser* that = reinterpret_cast<xml::parser*>( data );
	that->character_data( text, len );
}

void xml::parser::helper::comment_data_trampoline( void* data, char const* text )
{
	assert( data );
	xml::parser* that = reinterpret_cast<xml::parser*>( data );
	that->comment_data( text );
}

void xml::parser::helper::cdata_start_trampoline(void *data ) 
{
	assert( data );
	xml::parser* that = reinterpret_cast<xml::parser*>( data );
}

void xml::parser::helper::cdata_end_trampoline(void *data) 
{
	assert( data );
	xml::parser* that = reinterpret_cast<xml::parser*>( data );
}

//**********************************************************
// iostream
//----------------------------------------------------------

std::ostream& xml::operator<<( std::ostream& out, xml::expat_version )
{
	out << XML_ExpatVersion();
	return out;
}

//**********************************************************
// xml::parse_error
//----------------------------------------------------------

xml::parse_error::parse_error( char const* message, parser const& p )
: line_( p.current_line() ), column_( p.current_column() )
{
	assert( message && *message );
	std::ostringstream buffer;
	buffer << message << " (line " << line_ << ", column " << column_ << ")";
	message_ = buffer.str();
}

xml::parse_error::parse_error( std::string const& message, parser const& p )
: line_( p.current_line() ), column_( p.current_column() )
{
	assert( message.length() > 0 );
	std::ostringstream buffer;
	buffer << message << " (line " << line_ << ", column " << column_ << ")";
	message_ = buffer.str();
}

xml::parse_error::parse_error( char const* message, unsigned line, unsigned column )
: line_( line ), column_( column )
{
	assert( message && *message );
	std::ostringstream buffer;
	buffer << message << " (line " << line_ << ", column " << column_ << ")";
	message_ = buffer.str();
}

xml::parse_error::parse_error( std::string const& message, unsigned line, unsigned column )
: line_( line ), column_( column )
{
	assert( message.length() > 0 );
	std::ostringstream buffer;
	buffer << message << " (line " << line_ << ", column " << column_ << ")";
	message_ = buffer.str();
}

xml::parse_error::~parse_error() throw()
{
}

char const* xml::parse_error::what() const throw()
{
	assert( message_.length() > 0 );
	return message_.c_str();
}

