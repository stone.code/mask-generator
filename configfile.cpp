/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains the code to read and parse layout configuration 
** files.  This code is duplicated from layout2layout, but with some
** sections cut.  Configuration code for style information and darfield
** is not used by this software, and so ignored.  However, this code
** should largely reflect the same routines in layout2layout, and track
** any changes that occur there if the configuration format is updated.
**
*/

#pragma warning (disable:4786)

#include <layout.h>
#include "expatpp.h"
#include <assert.h>
#include <exception>
#include <fstream>
#include <string>

using namespace liblayout;
using std::string;

class layer_parser : public xml::parser
{
public:
	layer_parser(cif::map_id_name&	id_to_name_, cif::map_name_id& name_to_id_ ) 
		: id_to_name(id_to_name_), name_to_id(name_to_id_)
	{ state = XML_NULL; };

	void	element_start( const char *el, const char **attr );
	void	element_end( const char *el );
	void	character_data( const char* data, int len );
	
private:
	long			current_id;
	enum { XML_NULL, XML_LAYERS, XML_LAYER, XML_CIF, XML_STYLE } state;

	cif::map_id_name&	id_to_name;
	cif::map_name_id&	name_to_id;
};

void layer_parser::element_start(const char *el, const char **attr) 
{
	assert( depth() > 0 );

	switch ( state )
	{
	case XML_NULL:
	{
		static string const layers("layers");
		if ( depth()==1 && layers == el ) {
			state = XML_LAYERS;
		}
		else if ( depth()==1 ) {
			std::cerr << "Ignoring unknown top level element <" << el << ">." << std::endl;
		}
	}
	break;

	case XML_LAYERS:
	{
		static string const layer("layer");
		static string const style("style");

		if ( depth() == 2 && layer == el )
		{
			// move to next state
			state = XML_LAYER;
			
			// save the id
			char const* text = get_attribute( attr, "id" );
			if ( !text ) {
				std::cerr << "XML element <layer> missing ID attribute.  Defaulting to 0." << std::endl;
				current_id = 0;
			}
			else {
				current_id = atol( text );
			}
			
			/*
			// save the darkfield flags
			text = get_attribute( attr, "darkfield" );
			current_df = (text!=0);
			// set darkfield
			if ( current_df ) id_to_darkfield[current_id] = true;
			
			// set the styling information for this layer
			id_to_css[current_id] = default_style;
			*/
		}
		else if ( depth() == 2 && style == el )
		{
			// move to the next state
			state = XML_STYLE;
			
			// style not used by this program
			/*
			// update the style information
			char const* text = get_attribute( attr, "paint" );
			if ( text ) {
				default_style.set_fill( text );
				default_style.set_stroke( text );
			}
			text = get_attribute( attr, "fill" );
			if ( text ) default_style.set_fill( text );
			text = get_attribute( attr, "fillpattern" );
			if ( text ) default_style.set_fill_pattern( text );
			text = get_attribute( attr, "stroke" );
			if ( text ) default_style.set_stroke( text );
			text = get_attribute( attr, "strokepattern" );
			if ( text ) default_style.set_stroke_pattern( text );
			text = get_attribute( attr, "width" );
			if ( text ) default_style.set_stroke_width( text );
			text = get_attribute( attr, "join" );
			if ( text ) default_style.set_stroke_join( text );
			std::cout << "Default style set to " << default_style << std::endl;
			*/
		}
		else if ( depth() == 2 )
		{
			std::cerr << "Ignoring unknown element <" << el << ">." << std::endl;
		}
	}
	break;

	case XML_LAYER:
	{
		static string const elem_cif("cif");
		static string const elem_style("style");

		if ( depth() == 3 && elem_cif == el )
		{
			state = XML_CIF;
		}
		else if ( depth() == 3 && elem_style == el )
		{
			// move to the next state
			state = XML_STYLE;
			
			/*
			// get reference to layers style information
			style& current_style = id_to_css[current_id];
			
			// get the order
			char const* text = get_attribute( attr, "order" );
			if ( text ) current_style.order = atol( text );
			
			// update the style information
			text = get_attribute( attr, "paint" );
			if ( text ) {
				current_style.set_fill( text );
				current_style.set_stroke( text );
			}
			text = get_attribute( attr, "fill" );
			if ( text ) current_style.set_fill( text );
			text = get_attribute( attr, "fillpattern" );
			if ( text ) current_style.set_fill_pattern( text );
			text = get_attribute( attr, "stroke" );
			if ( text ) current_style.set_stroke( text );
			text = get_attribute( attr, "strokepattern" );
			if ( text ) current_style.set_stroke_pattern( text );
			text = get_attribute( attr, "width" );
			if ( text ) current_style.set_stroke_width( text );
			text = get_attribute( attr, "join" );
			if ( text ) current_style.set_stroke_join( text );
			*/
		}
		else if ( depth() == 3 )
		{
			std::cerr << "Ignoring unknown element <" << el << ">." << std::endl;
		}
	}
	break;

	default:
		std::cerr << "ERROR:  " << el << std::endl;
		assert( false );
	}
}

void layer_parser::character_data(const char* data, int len ) 
{
	switch ( state )
	{
	case XML_CIF:
	{
		string name( data, data+len );
		name_to_id[ name ] = current_id;
		if ( id_to_name.find(current_id) == id_to_name.end() ) id_to_name[current_id] = name;
	}
	break;

	};
}  

void layer_parser::element_end(const char *el) 
{
	switch ( state )
	{
	case XML_LAYERS:
		if ( depth() == 1 ) state = XML_NULL;
		break;

	case XML_LAYER:
		if ( depth() == 2 ) state = XML_LAYERS;
		break;

	case XML_CIF:
		if ( depth() == 3 ) state = XML_LAYER;
		break;

	case XML_STYLE:
		if ( depth() == 3 ) state = XML_LAYER;
		if ( depth() == 2 ) state = XML_LAYERS;
		break;
	}
}  

void read_layers( char const* filename, cif::map_name_id& name_to_id, cif::map_id_name& id_to_name )
{
	layer_parser parser( id_to_name, name_to_id  );

	// clear out all of the maps
	id_to_name.clear();
	name_to_id.clear();

	// read in contents of the file
	std::ifstream file;
	file.open( filename );
	if ( !file ) throw std::runtime_error( "Could not open the file for reading." );
	parser.parse( file );
	if ( !file.eof() ) throw std::runtime_error( "Could not read from the file." );
}
