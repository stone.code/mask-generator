/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This code duplicates the parser used by the layout generator to parse
** expressions used to describe the generated mask layers.  This code
** prints an RPN version of the expression to stdout.  This can be used
** debug expressions, but will also be used as part of the eventual
** web service.
*/

#include "layoutexpr.h"
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string>
#include <sstream>

using std::string;

/************************************************************************
** Parser
*/

static char const* advance_space( char const* lex )
{
	assert( lex );
	while ( isspace(*lex) ) ++lex;
	return lex;
}

static char const* advance_char( char const* lex, char token, char const* error_msg )
{
	lex = advance_space( lex );
	assert( lex );
	if ( *lex != token ) throw parse_error( lex, error_msg );
	return lex+1;
}

static char const* advance_uint( char const* lex, unsigned& lhs )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	if ( !isdigit(*lex ) ) throw parse_error( lex, "Expecting integer expression." );
	
	unsigned ret = *lex - '0';
	while ( isdigit( *++lex ) ) { ret = ret*10 + (*lex-'0'); }
	lhs = ret;
	
	return lex;
}

static char const* advance_maskid( char const* lex, std::ostream& rpn )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	if ( *lex == '@' ) {
		// Number identifier
		unsigned tmp;
		lex = advance_uint( lex+1, tmp );
		
		rpn << "ID#" << tmp << ' ';
	}
	else if ( isalpha(*lex) ) {
		char const* lex_begin = lex;
		++lex;
		while ( isalnum(*lex) ) ++lex;
		string name( lex_begin, lex );
		rpn << "ID'" << name << "' ";
	}
	else {
		throw parse_error( lex, "Expected mask id expression." );
	}

	return lex;
}

static char const* advance_expr( char const* lex, std::ostream& );

static char const* advance_factor( char const* lex, std::ostream& rpn )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	if ( *lex=='(' ) {
		++lex;
		lex = advance_expr( lex, rpn );
		assert( lex );
		lex = advance_char( lex, ')', "Expected matching bracked." );
		assert( lex );
	}
	else {
		lex = advance_maskid( lex, rpn );
		assert( lex );
	}

	return lex;
}

static char const* advance_term( char const* lex, std::ostream& rpn )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	lex = advance_factor( lex, rpn );
	assert( lex );

	do {
		lex = advance_space( lex );
		assert( lex );

		if ( *lex=='*' ) {
			++lex;
			lex = advance_factor( lex, rpn );
			rpn << "* ";
		}
		else if ( *lex=='^') {
			++lex;
			lex == advance_factor( lex, rpn );
			rpn << "^ ";
		}
		else {
			return lex;
		}
	} while(1);
}

static char const* advance_expr( char const* lex, std::ostream& rpn )
{
	assert( lex );
	lex = advance_space( lex );
	assert( lex );

	lex = advance_term( lex, rpn );
	assert( lex );

	do {
		lex = advance_space( lex );
		assert( lex );
		
		if ( *lex=='+' ) {
			++lex;
			lex = advance_space( lex );
			if ( isdigit(*lex) ) {
				unsigned tmp;
				lex = advance_uint( lex, tmp );
				rpn << tmp << ' ';
			}
			else {
				lex = advance_term( lex, rpn );
			}
			rpn << "+ ";
		}
		else if (*lex=='-') {
			++lex;
			lex = advance_space( lex );
			if ( isdigit(*lex) ) {
				unsigned tmp;
				lex = advance_uint( lex, tmp );
				rpn << tmp << ' ';
			}
			else {
				lex = advance_term( lex, rpn );
			}
			rpn << "- ";
		}
		else {
			return lex;
		}
	} while(1);
}

void parse_expression( char const* lex, std::ostream& rpn )
{
	lex = advance_maskid( lex, rpn );
	assert( lex );
	lex = advance_char( lex, '=', "Expected '='." );
	assert( lex );
	
	lex = advance_expr( lex, rpn );
	assert( lex );
	lex = advance_space( lex );
	if ( *lex ) throw parse_error( lex, "Expected end of expression." );

	rpn << '=';
}

int main( int argc, char* argv[] )
{
	for ( int lp=1; lp<argc; ++lp ) {
		try {
			std::stringstream rpn;
			parse_expression( argv[lp], rpn );
			printf( "%d: OK : %s\n", lp, rpn.str().c_str() );
		}
		catch ( parse_error& e ) {
			printf( "%d: Error : At %u, %s\n", lp, unsigned(e.lex()-argv[lp]), e.what() );
		}
	}
	
	return EXIT_SUCCESS;
}
